using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using CrudRazor.Models;
namespace CrudRazor.Pages.Curso
{
    public class CreateModel : PageModel
    {

        private readonly ApplicationDBContext _db;
        public CreateModel(ApplicationDBContext db)
        {
            _db = db;
        }

        [BindProperty]
        public CrudRazor.Models.Curso Curso { get; set; }
        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPost() 
        {
            if (!ModelState.IsValid)
                return Page();

            _db.Add(Curso);
            await _db.SaveChangesAsync();

            return RedirectToPage("Index");
        }
    }
}
