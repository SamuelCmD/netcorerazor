using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrudRazor.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace CrudRazor.Pages.Curso
{
    public class IndexModel : PageModel
    {

        private readonly ApplicationDBContext _db;
        public IndexModel(ApplicationDBContext db) {
            _db = db;
        }

        public IEnumerable<CrudRazor.Models.Curso> Cursos { get; set; }

        public async  Task OnGet()
        {
            Cursos = await _db.cursos.ToListAsync();
             
        }

        public async Task<IActionResult> OnPostDelete(int id)
        {
            var curso = await _db.cursos.FindAsync(id);
            if (curso == null) 
                return NotFound();

            _db.cursos.Remove(curso);
            await _db.SaveChangesAsync();

            return RedirectToPage("Index");
        }
    }
}
