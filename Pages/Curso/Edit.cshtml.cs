using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrudRazor.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace CrudRazor.Pages.Curso
{
    public class EditModel : PageModel
    {
        private readonly ApplicationDBContext _db;

        public EditModel(ApplicationDBContext db)
        {
            _db = db;
        }

        [BindProperty]
        public CrudRazor.Models.Curso Curso { get; set; }

        public async Task OnGet(int id)
        {
            Curso = await _db.cursos.FindAsync(id);
        }

        public async Task<IActionResult> OnPost()
        {
            if (!ModelState.IsValid)
                return Page();

            var cursoObtenido = await _db.cursos.FindAsync(Curso.Id);
            cursoObtenido.NombreCurso = Curso.NombreCurso;
            cursoObtenido.CantClases = Curso.CantClases;
            cursoObtenido.Precio = Curso.Precio;

            await _db.SaveChangesAsync();

            return RedirectToPage("Index");
        }
    }
}
