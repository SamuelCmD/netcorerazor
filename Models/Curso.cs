﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CrudRazor.Models
{
    public class Curso
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [Display(Name ="Nombre de Curso")]
        public string NombreCurso { get; set; }
        [Display(Name = "Cantidad de Clases")]
        public int CantClases { get; set; }
        public int Precio { get; set; }

    }
}
